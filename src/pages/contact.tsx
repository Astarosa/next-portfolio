import type { NextPage } from 'next';
import Title from '../components/Title';
import { useState, useEffect } from 'react';
import { useForm, SubmitHandler } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ErrorMessage } from '@hookform/error-message';
import { init, sendForm } from 'emailjs-com';
init(`${process.env.NEXT_PUBLIC_EMAILJS_USER_ID}`);

type Payload = {
  firstname: string;
  lastname: string;
  email: string;
  tel: number;
  message: string;
};

const Contact: NextPage = () => {
  const [contactNumber, setContactNumber] = useState('000000');

  const generateContactNumber = () => {
    const numStr = '000000' + ((Math.random() * 1000000) | 0);
    setContactNumber(numStr.substring(numStr.length - 6));
  };

  const {
    formState: { isSubmitting, isValid, errors },
    register,
    handleSubmit,
    setFocus,
    trigger,
    reset,
  } = useForm<Payload>({ mode: 'onChange' });

  useEffect(() => {
    setFocus('firstname');
  }, [setFocus]);

  const onSubmit: SubmitHandler<Payload> = async () => {
    generateContactNumber();
    await toast.promise(
      sendForm(
        `${process.env.NEXT_PUBLIC_EMAILJS_SERVICE_KEY}`,
        `${process.env.NEXT_PUBLIC_EMAILJS_TEMPLATE_KEY}`,
        '#contact-form'
      ),
      {
        pending: "Formulaire en cours d'envoie",
        success: 'Formulaire envoyé 👌',
        error: "Erreur lors de l'envoie du formulaire 🤯",
      }
    );
    reset();
  };

  return (
    <>
      <Title>Me Contacter</Title>
      <div className="container mx-auto px-5 py-10 flex flex-col flex-grow justify-center items-center">
        <ToastContainer />
        <form
          onSubmit={handleSubmit(onSubmit)}
          id="contact-form"
          className="w-full max-w-lg"
        >
          <input type="hidden" name="contact_number" value={contactNumber} />
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full md:w-1/2 px-3">
              <label
                htmlFor="firstname"
                className="block uppercase tracking-wide text-xs font-bold mb-2"
              >
                Prénom
              </label>
              <input
                type="text"
                id="firstname"
                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                {...register('firstname', {
                  required: 'Votre nom est requis',
                  maxLength: {
                    value: '30',
                    message: 'Votre nom ne peut exéder 30 caractères',
                  },
                })}
              />
              <ErrorMessage
                name="firstname"
                errors={errors}
                render={({ message }) => (
                  <div className="text-xs text-red-500 mt-1">{message}</div>
                )}
              />
            </div>
            <div className="w-full md:w-1/2 px-3">
              <label
                htmlFor="lastname"
                className="block uppercase tracking-wide text-xs font-bold mb-2"
              >
                Nom
              </label>
              <input
                type="text"
                id="lastname"
                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                {...register('lastname', {
                  required: 'Votre nom est requis',
                  maxLength: {
                    value: '30',
                    message: 'Votre nom ne peut exéder 30 caractères',
                  },
                })}
              />
              <ErrorMessage
                name="lastname"
                errors={errors}
                render={({ message }) => (
                  <div className="text-xs text-red-500 mt-1">{message}</div>
                )}
              />
            </div>
          </div>
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full md:w-1/2 px-3">
              <label
                htmlFor="email"
                className="block uppercase tracking-wide text-xs font-bold mb-2"
              >
                E-mail
              </label>
              <input
                type="email"
                id="email"
                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                {...register('email', {
                  required: 'Votre email est requis',
                  pattern: {
                    value:
                      /^(([^<>()[\]\\.,;:\s@']+(\.[^<>()[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    message: "Format d'email invalide",
                  },
                })}
              />
              <ErrorMessage
                name="email"
                errors={errors}
                render={({ message }) => (
                  <div className="text-xs text-red-500 mt-1">{message}</div>
                )}
              />
            </div>
            <div className="w-full md:w-1/2 px-3">
              <label className="block uppercase tracking-wide text-xs font-bold mb-2">
                Téléphone
              </label>
              <input
                type="tel"
                id="tel"
                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                {...register('tel', {
                  required: 'Votre numéro de téléphone est requis',
                  pattern: {
                    value: /^[0-9]{10}$/,
                    message: 'Numéro de téléphone invalide',
                  },
                })}
              />
              <ErrorMessage
                name="tel"
                errors={errors}
                render={({ message }) => (
                  <div className="text-xs text-red-500 mt-1">{message}</div>
                )}
              />
            </div>
          </div>
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full px-3">
              <label
                htmlFor="message"
                className="block uppercase tracking-wide text-xs font-bold mb-2"
              >
                Message
              </label>
              <textarea
                id="message"
                className="no-resize appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 h-48 resize-none"
                {...register('message', { required: true })}
              />
              {errors.message && (
                <div className="text-xs text-red-500 mt-1">Message requis</div>
              )}
            </div>
          </div>
          <div className="md:flex md:items-center md:justify-end">
            {/* <div
              className="g-recaptcha"
              data-sitekey={`${process.env.NEXT_PUBLIC_RECAPTCHA_PUBLIC_KEY}`}
            /> */}
            <button
              className="m-auto md:m-0 disabled:opacity-30 disabled:pointer-events-none uppercase shadow bg-yellow-500 hover:bg-yellow-600 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
              type="submit"
              onClick={() => trigger()}
              disabled={isSubmitting || !isValid}
            >
              Envoyer
            </button>
          </div>
        </form>
      </div>
    </>
  );
};

export default Contact;
