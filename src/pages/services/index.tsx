import type { NextPage } from 'next';
import NavLink from 'components/Navlink';
import Title from 'components/Title';

const Services: NextPage = () => {
  return (
    <div className="pb-24">
      <Title>Mes Services</Title>
      <section className="text-white body-font">
        <div className="container px-5 py-24 mx-auto">
          <div className="flex flex-col text-center w-full mb-24">
            <h2 className="sm:text-3xl text-2xl font-medium title-font">
              Ce que je vous propose
            </h2>
          </div>
          <div className="flex flex-wrap -m-4">
            <div className="p-4 md:w-1/2">
              <div className="flex rounded-lg h-full bg-gray-100 p-8 flex-col">
                <div className="flex items-center mb-3">
                  <div className="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-indigo-500 text-white flex-shrink-0">
                    <svg
                      fill="none"
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <path d="M22 12h-4l-3 9L9 3l-3 9H2"></path>
                    </svg>
                  </div>
                  <h2 className="text-gray-900 text-lg title-font font-medium">
                    Dévelopement Web et SEO
                  </h2>
                </div>
                <div className="flex-grow">
                  <p className="leading-relaxed text-gray-600">
                    Je développe votre site
                    internet avec une solution
                    adpatée à votre budget et vous
                    aide à optimiser votre
                    référencement naturel.
                  </p>
                  <NavLink href="/services/developpement-web-et-seo">
                    <div>
                      <a className="mt-3 text-yellow-600 inline-flex items-center">
                        En savoir plus
                      </a>
                      <svg
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        className="w-4 h-4 ml-2"
                        viewBox="0 0 24 24"
                      >
                        <path d="M5 12h14M12 5l7 7-7 7"></path>
                      </svg>
                    </div>
                  </NavLink>
                </div>
              </div>
            </div>
            <div className="p-4 md:w-1/2">
              <div className="flex rounded-lg h-full bg-gray-100 p-8 flex-col">
                <div className="flex items-center mb-3">
                  <div className="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-indigo-500 text-white flex-shrink-0">
                    <svg
                      fill="none"
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                      <circle
                        cx="12"
                        cy="7"
                        r="4"
                      ></circle>
                    </svg>
                  </div>
                  <h2 className="text-gray-900 text-lg title-font font-medium">
                    Hébergement et maintenance
                  </h2>
                </div>
                <div className="flex-grow">
                  <p className="leading-relaxed text-gray-600">
                    {`Je m'occupe d'héberger votre site selon vos besoins et en
                    assure sa maintenance tous les mois.`}
                  </p>
                  <NavLink href="/services/hebergement-et-maintenance">
                    <div>
                      <a className="mt-3 text-yellow-600 inline-flex items-center">
                        En savoir plus
                      </a>
                      <svg
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        className="w-4 h-4 ml-2"
                        viewBox="0 0 24 24"
                      >
                        <path d="M5 12h14M12 5l7 7-7 7"></path>
                      </svg>
                    </div>
                  </NavLink>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Services;
