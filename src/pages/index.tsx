import type { NextPage } from 'next';
import NavLink from 'components/Navlink';
import Image from 'next/image';

import Background from '../assets/images/home-header.jpg';

// Components
import DonutChart from '../components/DonutChart';

//Logos
import ReactSVGLogo from '../assets/images/react-logo.svg';
import StrapiSVGLogo from '../assets/images/strapi-logo.svg';
import WordpressSVGLogo from '../assets/images/wordpress-logo.svg';
import TailwindSVGLogo from '../assets/images/tailwind-css-logo.svg';
import JsSVGLogo from '../assets/images/js-logo.svg';
import GitSVGLogo from '../assets/images/git-logo.svg';

const Home: NextPage = () => {
  return (
    <>
      <section className="relative w-full h-screen flex items-center justify-center">
        <Image
          src={Background}
          alt="header-background"
          layout="fill"
          objectFit="cover"
          className="opacity-10 mix-blend-luminosity"
        />
        <div className="container mx-auto flex px-5 py-24 md:flex-row flex-col items-center z-10">
          <div className="lg:flex-grow flex flex-col justify-cente mb-16 mds:mb-0 items-center text-center">
            <h1 className="uppercase text-yellow-500 font-bold sm:text-8xl text-3xl mb-4">
              Baptiste COURGIBET
            </h1>
            <h2 className="flex flex-col uppercase text-sm md:text-lg tracking-widest mb-16 whitespace-pre">
              Développeur
              <span className="mt-1">React | Strapi | Wordpress</span>
            </h2>
            <div className="flex justify-center gap-4">
              <NavLink activeClassName="" href="/contact">
                <a className="transition duration-300 ease-in-out inline-flex border border-opacity-30 border-yellow-500 text-yellow-500 py-2 px-4 md:py-4 md:px-6 focus:outline-none hover:bg-yellow-500 hover:border-transparent hover:text-white select-none rounded uppercase tracking-wider font-semibold text-sm">
                  Me contacter
                </a>
              </NavLink>
              <a
                href='/CV_Baptiste_COURGIBET.pdf'
                target="_blank"
                rel="noreferrer"
                className="transition duration-300 ease-in-out inline-flex text-gray-400 border border-opacity-30 border-gray-400 py-2 px-4 md:py-4 md:px-6 focus:outline-none hover:bg-gray-700 hover:border-transparent hover:text-white cursor-pointer select-none rounded uppercase tracking-wider font-semibold text-sm"
              >
                Voir mon CV
              </a>
            </div>
          </div>
        </div>
      </section>
      <section className="border-b border-t border-gray-800">
        <div className="container px-5 py-10 lg:py-32 mx-auto max-w-3xl">
          <h3 className="uppercase text-sm md:text-2xl tracking-widest mb-8 md:mb-10 lg:mb-12">
            Qui suis-je ?
          </h3>
          <p className="text-lg font-light text-gray-400 mb-5">
            {`Je suis un développeur Full Stack basé à Lyon. 
            Je suis principalement orienté Javascript, c\'est sur ce langage que j\'ai fait mes armes. 
            À l\'aise en télétravail je n\'en reste pas moins quelqu\'un de sociable quand on me connait et que je connais bien les personnes avec qui je travail.`}
          </p>
          <p className="text-lg font-light text-gray-400">
            {`Les métiers du web sont vastes et plus ou moins complexes.
            J'entretiens donc mon savoir quotidiennement afin de rester à jour
            des technologies que j'utilise et je fais régulièrement de la veille
            afin d'apprendre de nouvelles compétences. Grand passionné de tech
            notamment Informatique, Spatiale, Énergie, mes centres d'intérêt
            tournent autour de sciences en tout genre et de leur macro économie.`}
          </p>
        </div>
      </section>
      <section className="bg-gray-900 border-b border-gray-800">
        <div className="container px-5 py-10 lg:py-32 mx-auto">
          <h3 className="uppercase text-sm md:text-2xl tracking-widest mb-8 md:mb-10 lg:mb-12">
            Mes compétences
          </h3>
          <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-6 gap-5 md:gap-10 lg:gap-20">
            <DonutChart ratio={70} label="JavaScript">
              <JsSVGLogo className="fill-current" />
            </DonutChart>
            <DonutChart ratio={70} label="ReactJs">
              <ReactSVGLogo className="fill-current" />
            </DonutChart>
            <DonutChart ratio={60} label="TailwindCss">
              <TailwindSVGLogo className="fill-current" />
            </DonutChart>
            <DonutChart ratio={70} label="Git">
              <GitSVGLogo className="fill-current" />
            </DonutChart>
            <DonutChart ratio={60} label="Wordpress">
              <WordpressSVGLogo className="fill-current" />
            </DonutChart>
            <DonutChart ratio={30} label="Strapi">
              <StrapiSVGLogo className="fill-current" />
            </DonutChart>
          </div>
        </div>
      </section>
      <section className="z-10 relative bg-gray-900">
        <div className="container px-5 py-10 lg:py-32 mx-auto">
          <h3 className="uppercase text-sm md:text-2xl tracking-widest mb-8 md:mb-10 lg:mb-12">
            Mon parcours
          </h3>
          <div className="grid grid-cols-1 lg:grid-cols-2 lg:gap-20">
            <div>
              <p className="text-lg font-light text-gray-400 mb-5">
                {`Après un bac en sciences économiques et sociales j'ai commencé
                mes études en école de commerce à l'Institut Supérieur de
                Gestion de Lyon de laquelle je suis sorti avec un Bachelor en
                Business et Management et un diplôme d'auto-entreprenariat de
                l'Université Laval (Québec).`}
              </p>
              <p className="text-lg font-light text-gray-400 mb-5">
                {`Ayant toujours eu un très fort attrait pour le monde de
                l'informatique et du web, j'ai entamé ma reconversion en 2018.
                D'abord un an à Digital Campus afin d'acquérir des connaissances
                globales, je me rend compte que ce qui me plais plus que tout
                c'est le développement web. J'en ressort avec de bonnes
                connaissances en intégration (HTML/CSS/JQuery) mais je prend conscience
                des limites d'une école généraliste et décide de me
                spécialiser.`}
              </p>
            </div>
            <div>
              <p className="text-lg font-light text-gray-400 mb-5">
                {`C'est en 2019 que j'arrive à la Wild Code School, spécialisation
                React | NodeJs. J'y effectue une formation de 5 mois, 70% React,
                30 % NodeJs tout cela dans le cadre de 3 grands projets en
                méthodologie Agile SCRUM. Très satisfait de cette formation, je
                décide ensuite de faire l'alternance d'un an qui suit, afin de
                consolider mes connaissances et de prendre de l'expérience de
                terrain. Je suis donc engagé chez l'Agence 33 Degrés pour an. 3
                semaines en entreprise où je développe et maintient des sites
                Wordpress, leur Backups, la gestion de DNS, les migrations sur
                de nouveaux serveurs, un Google Workspace, bref la liste est
                longue, on ne s'ennuie pas en Agence.`}
              </p>
              <p className="text-lg font-light text-gray-400 mb-5">
                {`La semaine restante j'étais en formation à la Wild Code School
                où j'approfondissais mes connaissance en React et Node tout en y
                ajoutant de nouvelles cordes à mon arc. J'y apprend React
                Native, le React pour mobile et Graphql, un langage que
                j'utilise maintenant avec mes API pour faire mes requêtes.`}
              </p>
            </div>
          </div>
        </div>
        <div className="container px-5 pb-10 lg:pb-32 mx-auto max-w-3xl">
          <p className="text-lg lg:text-2xl lg:text-center lg:leading-9 font-light text-gray-400 mb-5">
            {`Ces expériences m\'ont permis d\'acquérir un bagage technique varié et riche en contenu. 
            Mon objectif est désormais de consolider mes acquis pour devenir expert dans ces technologies.`}
          </p>
        </div>
      </section>
    </>
  );
};

export default Home;
