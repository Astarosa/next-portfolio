import Title from 'components/Title';

const WebHostingMaintenance = () => {
  return <Title>Hébergement et maintenance</Title>;
};

export default WebHostingMaintenance;
