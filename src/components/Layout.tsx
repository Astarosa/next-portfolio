import React, { ReactNode } from 'react';
import Head from 'next/head';

//components
import Header from './Header';
import Footer from './Footer';

type Props = {
  children?: ReactNode;
};

const Layout = ({ children }: Props) => {
  return (
    <div className="bg-gray-900 text-white flex flex-col min-h-screen">
      <Head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon.ico" />
        <title>Baptiste COURGIBET Développeur Web React</title>
      </Head>
      <Header />
      <main>{children}</main>
      <Footer />
    </div>
  );
};

export default Layout;
