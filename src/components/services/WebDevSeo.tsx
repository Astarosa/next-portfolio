import Title from 'components/Title';

const WebDevSeo = () => {
  return <Title>Développement web et SEO</Title>;
};

export default WebDevSeo;
