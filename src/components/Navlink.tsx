import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import Link from 'next/link';
import React, { Children } from 'react';

interface Props {
  activeClassName?: string;
  href: string;
  as?: string;
}

const NavLink: React.FC<Props> = ({ children, activeClassName, ...props }) => {
  const { asPath } = useRouter();
  const child: any = Children.only(children);
  const childClassName = child.props.className || '';

  // pages/index.js will be matched via props.href
  // pages/about.js will be matched via props.href
  // pages/[slug].js will be matched via props.as
  const className =
    asPath === props.href || asPath === props.as
      ? `${childClassName} ${activeClassName}`.trim()
      : childClassName;

  return (
    <Link {...props}>
      {React.cloneElement(child, {
        className: className || null,
      })}
    </Link>
  );
};

NavLink.propTypes = {
  activeClassName: PropTypes.string.isRequired,
};

export default NavLink;
