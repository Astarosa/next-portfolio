import NavLink from './Navlink';

import Logo from '../assets/images/logo.svg';
import LogoLinkedIn from '../assets/images/logo-linkedin.svg';
import LogoMalt from '../assets/images/logo-malt.svg';
import LogoGitlab from '../assets/images/logo-gitlab.svg';

const Footer = () => {
  return (
    <footer className="z-10 relative border-t border-gray-800 text-white body-font w-full h-24 flex items-center">
      <div className="container px-5 mx-auto flex items-center justify-between sm:flex-row flex-col">
        <div className="flex">
          <NavLink href="/">
            <a className="flex title-font font-medium items-center">
              <Logo className="w-5 fill-current" />
            </a>
          </NavLink>
          <p className="uppercase sm:ml-6 sm:pl-6 sm:border-l sm:border-gray-600 sm:py-2 ml-2 tracking-wider text-sm">
            © {new Date().getFullYear()} - Baptiste COURGIBET
          </p>
        </div>
        <span className="inline-flex sm:mt-0 mt-4 justify-center sm:justify-start">
          <a
            href="https://www.linkedin.com/in/baptiste-courgibet/"
            target="_blank"
            rel="noreferrer"
          >
            <LogoLinkedIn className="w-5 fill-current" />
          </a>
          {/* <a
            href="https://www.malt.fr/profile/baptistecourgibet"
            target="_blank"
            rel="noreferrer"
            className="ml-4"
          >
            <LogoMalt className="w-5 fill-current" />
          </a> */}
          <a
            href="https://gitlab.com/Astarosa"
            target="_blank"
            rel="noreferrer"
            className="ml-4"
          >
            <LogoGitlab className="w-5 fill-current" />
          </a>
        </span>
      </div>
    </footer>
  );
};

export default Footer;
