module.exports = {
  singleQuote: true,
  semiColons: true,
  bracketSpacing: true,
};
