import { CSSProperties, useRef } from 'react';
import useOnScreen from '../hooks/useOnScreen';

interface DonutChartProps {
  ratio: number;
  label: string;
}

const DonutChart: React.FC<DonutChartProps> = ({ children, ratio, label }) => {
  const config = {
    radius: 40,
    strokeWidth: 3,
    duration: 500,
    delay: 0,
    max: 100,
  };

  const halfCircle = config.radius + config.strokeWidth;
  const circleCircumference = 2 * Math.PI * config.radius;

  const ref = useRef<HTMLDivElement | null>(null);
  const onScreen = useOnScreen(ref);

  return (
    <div
      ref={ref}
      className="relative flex items-center justify-center text-yellow-500 mb-12"
    >
      <svg
        width={'100%'}
        height={'100%'}
        viewBox={`0 0 ${halfCircle * 2}  ${halfCircle * 2}`}
      >
        <g>
          <circle
            className="stroke-current"
            cx="50%"
            cy="50%"
            strokeWidth={config.strokeWidth}
            r={config.radius}
            fill="transparent"
            strokeOpacity={0.2}
          />
          {onScreen && (
            <circle
              className="animate-dash stroke-current"
              style={
                {
                  '--dashoffset': circleCircumference / (100 / (100 - ratio)),
                  '--dashoffset-from': circleCircumference,
                } as CSSProperties
              }
              cx="50%"
              cy="50%"
              strokeWidth={config.strokeWidth}
              r={config.radius}
              fill="transparent"
              strokeDasharray={circleCircumference}
              strokeLinecap="round"
            />
          )}
        </g>
      </svg>
      <div className="absolute flex items-center justify-center w-2/5 h-2/5">{children}</div>
      <div className="absolute top-full mt-5">{label}</div>
    </div>
  );
};

export default DonutChart;
