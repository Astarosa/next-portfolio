/** @type {import('next').NextConfig} */

module.exports = {
  i18n: {
    locales: ['fr'],
    defaultLocale: 'fr',
  },
  reactStrictMode: true,
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });

    return config;
  },
};
