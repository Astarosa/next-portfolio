const colors = require('tailwindcss/colors');

module.exports = {
  purge: ['./src/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      height: {
        'screen-content': 'calc(100vh - 6rem)',
      },
      fontFamily: {
        sans: ['barlow', 'sans-serif'],
        icon: ['Material Icons Outlined', 'sans-serif'],
      },
      colors: {
        gray: colors.trueGray,
        teal: colors.teal
      },
      animation: {
        dash: 'dash 1s ease-out forwards',
      },
      keyframes: {
        dash: {
          from: {
            'stroke-dashoffset': 'var(--dashoffset-from)',
          },
          to: {
            'stroke-dashoffset': 'var(--dashoffset)',
          },
        },
      },
    },
  },
  variants: {
    extend: {
      transitionProperty: ['responsive', 'motion-safe', 'motion-reduce'],
      opacity: ['disabled'],
      pointerEvents: ['disabled'],
    },
  },
  plugins: [],
};
