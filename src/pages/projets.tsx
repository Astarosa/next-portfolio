import { NextPage } from 'next';
import Title from 'components/Title';

const Projets: NextPage = () => {
  return (
    <div className="pb-24">
      <Title>Projets</Title>
      <section className="text-white body-font">
        <div className="container px-5 py-24 md:py-36 mx-auto">
          <div className="flex flex-col text-center w-full">
            <h2 className="sm:text-3xl text-2xl font-medium title-font">
              Cette page est encore en cours de construction mais promis, ils arrivent bientôt !
            </h2>
            <p className="mt-20">{'En attendant voici un lien vers mon'}{<a href="https://gitlab.com/Astarosa" target="_blank" rel="noreferrer noopener" className="text-yellow-500 md:hover:underline"> Gitlab</a>}{'. La plupart étant privés, je vais progressivement les passer en public dès lors qu\'ils seront migrés depuis Github'}</p>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Projets;
