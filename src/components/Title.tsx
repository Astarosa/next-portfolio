const Title: React.FC = ({children}) => {
  return (
    <div className='flex flex-col justify-end w-full h-60 lg:h-80 border-b border-gray-800'>
      <div className='container mx-auto px-5 pb-10'>
        <h1 className='uppercase text-white font-bold sm:text-6xl text-3xl mb-4'>
          {children}
        </h1>
      </div>
    </div>
  )
}

export default Title;