import { Fragment, useState, useEffect, useRef } from 'react';
import NavLink from './Navlink';
import { Menu, Transition } from '@headlessui/react';
import Logo from '../assets/images/logo.svg';

const MainNav = () => {
  return (
    <div className="hidden md:block">
      <nav className="md:ml-auto flex flex-wrap items-center text-base justify-center gap-8">
        <NavLink activeClassName="text-yellow-500" href="/">
          <a className="transition duration-300 hover:text-yellow-500 uppercase tracking-wider font-semibold text-sm">
            À propos
          </a>
        </NavLink>
        {/* <NavLink activeClassName="text-yellow-500" href="/services">
          <a className="transition duration-300 hover:text-yellow-500 uppercase tracking-wider font-semibold text-sm">
            Services
          </a>
        </NavLink> */}
        <NavLink activeClassName="text-yellow-500" href="/projets">
          <a className="transition duration-300 hover:text-yellow-500 uppercase tracking-wider font-semibold text-sm">
            Projets
          </a>
        </NavLink>
        {/* <NavLink activeClassName="text-yellow-500" href="/contact">
          <a className="transition duration-300 hover:text-yellow-500 uppercase tracking-wider font-semibold text-sm">
            Contact
          </a>
        </NavLink> */}
      </nav>
    </div>
  );
};

const MobileNav = () => {
  return (
    <div className="block md:hidden">
      <Menu>
        {({ open }) => (
          <>
            <Menu.Button>
              <span className="font-icon text-2xl mt-2">
                {`${open ? 'menu_open' : 'menu'}`}
              </span>
            </Menu.Button>
            <Transition
              as={Fragment}
              enter="transition ease-out duration-100"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              <Menu.Items className="flex flex-col justify-center fixed left-0 w-full mt-2 bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                <div className="px-1 py-1 ">
                  <Menu.Item>
                    {({ active }) => (
                      <NavLink activeClassName="text-yellow-500" href="/">
                        <a
                          className={`${active
                            ? 'bg-yellow-500 text-white'
                            : 'text-gray-900'
                            } group flex rounded-md items-center w-full px-2 py-2 font-semibold text-sm`}
                        >
                          À propos
                        </a>
                      </NavLink>
                    )}
                  </Menu.Item>
                  {/* <Menu.Item>
                    {({ active }) => (
                      <NavLink
                        activeClassName="text-yellow-500"
                        href="/services"
                      >
                        <a
                          className={`${
                            active
                              ? 'bg-yellow-500 text-white'
                              : 'text-gray-900'
                          } group flex rounded-md items-center w-full px-2 py-2 font-semibold text-sm`}
                        >
                          Services
                        </a>
                      </NavLink>
                    )}
                  </Menu.Item> */}
                  <Menu.Item>
                    {({ active }) => (
                      <NavLink
                        activeClassName="text-yellow-500"
                        href="/projets"
                      >
                        <a
                          className={`${active
                            ? 'bg-yellow-500 text-white'
                            : 'text-gray-900'
                            } group flex rounded-md items-center w-full px-2 py-2 font-semibold text-sm`}
                        >
                          Projets
                        </a>
                      </NavLink>
                    )}
                  </Menu.Item>
                  {/* <Menu.Item>
                    {({ active }) => (
                      <NavLink
                        activeClassName="text-yellow-500"
                        href="/contact"
                      >
                        <a
                          className={`${active
                            ? 'bg-yellow-500 text-white'
                            : 'text-gray-900'
                            } group flex rounded-md items-center w-full px-2 py-2 font-semibold text-sm`}
                        >
                          Contact
                        </a>
                      </NavLink>
                    )}
                  </Menu.Item> */}
                </div>
              </Menu.Items>
            </Transition>
          </>
        )}
      </Menu>
    </div>
  );
};

const Header = () => {
  const [backgroundOpacity, setBackgroundOpacity] = useState(0);

  const ref = useRef<HTMLElement>(null);

  useEffect(() => {
    if (!ref.current) return;
    const onScroll = () => {
      const scrollY = window.scrollY;
      const headerHeight = ref.current!.offsetHeight * 2;
      setBackgroundOpacity(
        headerHeight && scrollY <= headerHeight ? scrollY / headerHeight : 1
      );
    };
    window.addEventListener('scroll', onScroll);
    onScroll();
    return () => window.removeEventListener('scroll', onScroll);
  }, [ref]);

  return (
    <header ref={ref} className="fixed w-full top-0 z-30">
      <div
        style={{ opacity: backgroundOpacity }}
        className="absolute z-0 w-full h-full bg-gray-800"
      />
      <div className="container mx-auto flex px-5 py-5 lg:py-10 justify-between items-center z-10 relative">
        <NavLink activeClassName="text-white" href="/">
          <a className="flex title-font font-medium items-center">
            <Logo className="w-8 fill-current" alt="logo-baptiste-courgibet" />
          </a>
        </NavLink>
        <MainNav />
        <MobileNav />
      </div>
    </header>
  );
};

export default Header;
