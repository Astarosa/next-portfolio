import NotFoundBackground from '../assets/images/404.svg';

const NotFound: React.FC = () => {
  return (
    <div className="relative overflow-hidden h-screen-content">
      <div className='fixed inset-0 flex justify-center items-center'>
        <NotFoundBackground
          className="absolute object-cover min-w-full min-h-full filter grayscale"
          alt="not-found-background"
        />
      </div>
      <div className="container h-full mx-auto px-6 md:px-12 relative z-10 flex items-center py-32 xl:py-40">
        <div className="w-full font-mono flex flex-col items-center relative z-10 transform transform-y-2/5">
          <h1 className="font-extrabold text-5xl text-center text-white leading-tight mt-4">
            Vous êtes seul ici
          </h1>
          <p className="font-extrabold text-8xl mt-10 text-white animate-bounce">
            404
          </p>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
