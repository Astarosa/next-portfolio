import { useEffect } from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';

//Services
import WebDevSeo from 'components/services/WebDevSeo';
import WebHostingMaintenance from 'components/services/WebHostingMaintenance';

enum slug {
  'developpement-web-et-seo' = 'developpement-web-et-seo',
  'hebergement-et-maintenance' = 'developpement-web-et-seo'
}

const Service: NextPage = () => {
  const router = useRouter();
  const { service } = router.query;
  useEffect(() => {
    if (!Object.values(slug).includes(service as any)) {
      router.push('/404');
    }
  }, []) //eslint-disable-line
  switch (service) {
    case slug['developpement-web-et-seo']:
      return <WebDevSeo />;
    case slug['hebergement-et-maintenance']:
      return <WebHostingMaintenance />;
    default: {
      return null;
    }
  }
};

export default Service;
